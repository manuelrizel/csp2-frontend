//CAPTURE the html body container which will display the content coming from the database.
let modalButton = document.querySelector('#adminButton')
let container = document.querySelector('#coursesContainer')

//we are going to take the value of the isAdmin property from the local storage.
let isAdmin = localStorage.getItem("isAdmin")
let cardFooter;


if(isAdmin == "false" || !isAdmin){
	//if a user is a regular user, do not show the addcourse button.
	modalButton.innerHTML = null;
}else {
	modalButton.innerHTML = `
		<div class="col-md-2 offset-md-10"><a href="./addCourse.html" class="btn btn-block btn-success">Add Course</a>
		</div>
	`
}

fetch('https://pure-caverns-93135.herokuapp.com/api/courses/').then(res => res.json()).then(data => {
	console.log(data);

	//declare a variable that will display a result in the browser depending on the return
	let courseData;
	//create a control structure that will determine the value that the variable will hold.
	if(data.length < 1){
		courseData = "No course available."
	}else {
		//we will iterate the courses collection and display each course inside the browser.
		courseData = data.map(course => {
			//lets check the make up of each element inside the courses collection
			console.log(course._id)

			//if the user is a regular user, display the enroll button and display course button.
			if(isAdmin == "false" || !isAdmin){
				cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>
					<a href="" class="btn btn-dark text-white btn-block">Enroll</a>
				`
			}else {
				cardFooter = `<a href="./editCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block"> Edit </a>
				<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-dark text-white btn-block">Disable Course</a>`
			}
			return (
				`<div class="col-md-4 my-3">
					<div class="card sketchy">
						<div class="card-body ">
							<h5 class="card-title"> ${course.name} </h5>
							<p class="card-text text-left">
							${course.description}
							</p>
							<p class="card-text text-left">
							${course.price}
							</p>
							<p class="card-text text-left">
							${course.createdOn}
							</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>` //we attached a query string in the url which allows us to embed the ID from the database record into the query string.
				//? -> inside the url "acts" as a "separator", it indicates the end of a url resourse path and indicates the start of the *query string*.
				// # -> this was originally used to jump to a specific element with the same id name/value.
			)
		}).join("")//we used the join() to create a return of a new string
		//it concatenated all the objects inside the array and converted each to a string data type.
	}
	container.innerHTML = courseData;
})