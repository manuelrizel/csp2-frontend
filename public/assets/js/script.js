console.log("hello from script")
//capture the navSession element inside the navbar component
let navItem = document.querySelector('#navSession') 

//lets take the access token from the local storage property.
let userToken = localStorage.getItem("token")
//console.log(userToken)


if(!userToken) {
   navItem.innerHTML =
   `
    <li class="nav-item">
    	<a href="./login.html" class="nav-link">Log in</a>
    </li>
   `
}else{
   navItem.innerHTML =
   `
   <li class="nav-item">
   	    <a href="./logout.html" class="nav-link">Log Out</a>
   </li>
   `
}
